----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:02:31 01/15/2016 
-- Design Name: 
-- Module Name:    top - structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
	generic (
		total_data_width : positive := 6; -- Numero de registros * Ancho de cada registro
		data_width 	  : positive := 3;
		counter_width : positive := 2
	);
   port ( 
		clock 	: in  STD_LOGIC;
		reset_n	: in  STD_LOGIC;
		data		: in  STD_LOGIC;
		pause 	: in  STD_LOGIC;
		direction : in  STD_LOGIC;
		serial_out 		: out STD_LOGIC;
		serial_out_clk : out STD_LOGIC;
		register_clk 	: out STD_LOGIC;
		display 	: out  STD_LOGIC_VECTOR (6 downto 0);
		display_select : out  STD_LOGIC_VECTOR (3 downto 0)
	);
end top;

architecture structural of top is
	--Registro desplazamiento bidireccional
	component bi_shift_register 
	generic (
		width : positive
	);
	port (
		reset_n  : in  STD_LOGIC;
		clk 	 : in  STD_LOGIC;
		mode 	 : in  STD_LOGIC_VECTOR (1 downto 0);
		left_in  : in  STD_LOGIC;
		right_in : in  STD_LOGIC;
		left_out  : out STD_LOGIC;
		right_out : out  STD_LOGIC;
		q 		 : out  STD_LOGIC_VECTOR (width - 1 downto 0)		
	);
	end component;
	--Contador binario
	component ring_counter 
	generic (
		width: positive
	);
	port (
		clk 	: in  STD_LOGIC;
		reset_n : in  STD_LOGIC;
		q 		: out  STD_LOGIC_VECTOR (width - 1 downto 0)
	);	
	end component;
	--Decodificador octal a 7 segmentos
	component Decodificador
	port ( 
		code : in  STD_LOGIC_VECTOR (2 downto 0);
		led  : out  STD_LOGIC_VECTOR (6 downto 0)
	);	
	end component;
	--Multiplexor 2x1 de 3 bit
	component Multiplexor
	generic (
		width: positive
	);
	port ( 
		a : in  STD_LOGIC_VECTOR (2 downto 0);
		b : in  STD_LOGIC_VECTOR (2 downto 0);
		selec 	: in  STD_LOGIC;
		salida 	: out  STD_LOGIC_VECTOR (2 downto 0)
	);
	end component;
	--Prescaler
	component prescaler is
	port ( 
		clk_in : in  std_logic;
		rst       : in  std_logic;
		clk_out   : out std_logic
	);
	end component;
	--Registro paralelo a serie
	component paralelo_serie is
	generic (
		width: positive
	);
	port ( 
		clk 		: in  STD_LOGIC;
		reset_n 	: in  STD_LOGIC;
		parallel_in : in  STD_LOGIC_VECTOR (width - 1 downto 0);
		serial_out 	: out  STD_LOGIC;
		done 		: out  STD_LOGIC
	);
	end component;

	signal clk_contador	: STD_LOGIC;
	signal clk_registros : STD_LOGIC;
	signal clk_PISO : STD_LOGIC;
	signal com_1_2  : STD_LOGIC; 
	signal com_2_1  : STD_LOGIC;
	signal control_reg 	: STD_LOGIC_VECTOR (1 downto 0);
	signal salida_contador : STD_LOGIC_VECTOR (3 downto 0) := (others => '1');
	signal salida_reg_1 	: STD_LOGIC_VECTOR (data_width - 1 downto 0);
	signal salida_reg_2 	: STD_LOGIC_VECTOR (data_width - 1 downto 0);
	signal entrada_PISO	: STD_LOGIC_VECTOR (total_data_width - 1 downto 0);
	signal salida_mux		: STD_LOGIC_VECTOR (data_width - 1 downto 0);

begin
	--Registro donde se encuentra el MSB
	Inst_reg0: bi_shift_register
		generic map (
			width => data_width
		)
		port map (
			reset_n => reset_n, 
			clk => clk_registros,
			mode => control_reg,
			left_in => data,
			right_in => com_2_1,
			left_out => open,	--no conectado
			right_out => com_1_2,
			q => salida_reg_1
		);
	--Registro donde se encuentra el LSB
	Inst_reg1: bi_shift_register 
		generic map (
			width => data_width
		)
		port map (
			reset_n => reset_n, 
			clk => clk_registros,
			mode => control_reg,
			left_in => com_1_2,
			right_in => data,
			left_out => com_2_1,
			right_out => open, --no conectado
			q => salida_reg_2
		);
	Inst_selector : ring_counter
		generic map (
			width => counter_width
		)
		port map (
			clk => clk_contador, 
			reset_n => reset_n, 
			q => salida_contador(3 downto 2)
		);
	Inst_decoder : Decodificador 
		port map (
			code => salida_mux, 
			led => display
		);		
	Inst_multiplexer : Multiplexor
		generic map (
			width => data_width
		)
		port map (
			a => salida_reg_1,
			b => salida_reg_2, 
			selec => salida_contador(2),
			salida => salida_mux
		);
	Inst_clk_counter : entity work.prescaler(high_freq)
		port map (
			clk_in => clock, 
			reset_n => reset_n, 
			clk_out => clk_contador
		);
	Inst_clk_PISO : entity work.prescaler(low_freq)
		port map (
			clk_in => clock, 
			reset_n => reset_n, 
			clk_out => clk_PISO
		);
	Inst_PISO_register : paralelo_serie
		generic map (
			width => total_data_width
		)
		port map (
			clk => clk_PISO,
			reset_n => reset_n,
			parallel_in => entrada_PISO,
			serial_out => serial_out,
			done => clk_registros
		);

	control_reg(0) <= '1' when pause = '0' and direction = '0' else '0';
	control_reg(1) <= '1' when pause = '0' and direction = '1' else '0';

	entrada_PISO <= salida_reg_1 & salida_reg_2;
	display_select <= salida_contador;
	register_clk <= clk_registros;
	serial_out_clk <= clk_PISO;
end structural;

