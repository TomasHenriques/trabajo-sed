library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity prescaler is
  port (
    clk_in    : in  std_logic;
    reset_n   : in  std_logic;
    clk_out   : out std_logic);
end prescaler;

architecture high_freq of prescaler is
  signal counter : unsigned(12 downto 0); -- logaritmo en base 2 de factor redondeando el resultado hacia arriba
  signal clk_out_i : std_logic;
begin
  process (clk_in, reset_n)
  begin
    if reset_n = '0' then
      clk_out_i   <= '0';
      counter   <= (others => '0');
    elsif clk_in'event and clk_in = '1' then
      if counter = X"1388" then     -- factor = 5 000 in hex
        counter   <= (others => '0');
        clk_out_i   <= not clk_out_i;
      else
        counter <= counter + "1";
      end if;
    end if;
  end process ;
clk_out <= clk_out_i;
end high_freq;

architecture low_freq of prescaler is
  signal counter : unsigned(24 downto 0); -- logaritmo en base 2 de factor redondeando el resultado hacia arriba
  signal clk_out_i : std_logic;
begin
  process (clk_in, reset_n)
  begin
    if reset_n = '0' then
      clk_out_i   <= '0';
      counter   <= (others => '0');
    elsif clk_in'event and clk_in = '1' then
      if counter = X"989680" then     -- factor = 10 000 000 in hex
        counter   <= (others => '0');
        clk_out_i   <= not clk_out_i;
      else
        counter <= counter + "1";
      end if;
    end if;
  end process;
clk_out <= clk_out_i;
end low_freq;