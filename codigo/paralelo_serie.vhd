----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:22:59 01/25/2016 
-- Design Name: 
-- Module Name:    paralelo_serie - behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity paralelo_serie is
	generic (
		width : positive := 6
	);
	port ( 
		clk 		: in  STD_LOGIC;
		reset_n 	: in  STD_LOGIC;
		parallel_in : in  STD_LOGIC_VECTOR (width - 1 downto 0);
		serial_out 	: out  STD_LOGIC;
		done 		: out  STD_LOGIC
	);
end paralelo_serie;

architecture behavioral of paralelo_serie is
	signal reg : STD_LOGIC_VECTOR (width - 1 downto 0);
	signal counter : STD_LOGIC_VECTOR (width - 1 downto 0);
	signal done_i : STD_LOGIC;
	
begin
	process(clk, reset_n)
	begin
		if reset_n = '0' then
			reg <= (others => '0');
			counter <= (others => '0');
		elsif	clk'event and clk = '1' then
			if done_i = '1' then
				reg <= parallel_in;
				counter <= (others => '1');
			else 
				reg <= reg(reg'left - 1 downto 0) & '0';
				counter <= '0' & counter(counter'left downto 1);
			end if;
		end if;
	end process;
	done_i <= '1' when counter(0) = '0' else '0';
	serial_out <= reg(width - 1);
	done <= done_i;
end behavioral;

