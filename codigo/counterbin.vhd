----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:16:51 10/09/2015 
-- Design Name: 
-- Module Name:    counterbin - behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ring_counter is
   generic (
		width: positive := 4
	);
	port (
		clk : in  STD_LOGIC;
		reset_n : in  STD_LOGIC;
		q : out  STD_LOGIC_VECTOR (width - 1 downto 0)
	);
end ring_counter;

architecture behavioral of ring_counter is
	signal q_i: STD_LOGIC_VECTOR(q'range);
begin
	process (clk, reset_n)
	begin
		assert width >= 2 
			report "LA DIMENSION DEL CONTADOR DEBE SER 2 COMO MINIMO"
			severity error;
			
		if reset_n = '0' then
			q_i(width - 2 downto 0) <= (others => '0');
			q_i(width - 1) <= '1';
		elsif clk'event and clk = '1' then
			q_i <= q_i(0) & q_i(width - 1 downto 1);
		end if;
	end process;
	q <= STD_LOGIC_VECTOR(q_i);
end behavioral;

