----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:13:48 12/11/2015 
-- Design Name: 
-- Module Name:    Multiplexor - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Multiplexor is
    Generic ( width: positive := 3 );
	 Port ( a : in  STD_LOGIC_VECTOR (width - 1 downto 0);
           b : in  STD_LOGIC_VECTOR (width - 1 downto 0);
           selec : in  STD_LOGIC;
           salida : out  STD_LOGIC_VECTOR (width - 1 downto 0));
end Multiplexor;

architecture dataflow of Multiplexor is
	
begin
	
	salida <= a when selec = '0' else b;

end dataflow;

