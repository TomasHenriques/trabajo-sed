--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   10:58:07 01/25/2016
-- Design Name:   
-- Module Name:   C:/Users/Tom/Documents/VHDL/TrabajoSED/trabajo-sed/codigo/Testbench/paralelo_serie_tb.vhd
-- Project Name:  ISE_trabajo
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: paralelo_serie
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY paralelo_serie_tb IS
END paralelo_serie_tb;
 
ARCHITECTURE behavior OF paralelo_serie_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT paralelo_serie
    PORT(
         clk : IN  std_logic;
         reset_n : IN  std_logic;
         parallel_in : IN  std_logic_vector(5 downto 0);
         serial_out : OUT  std_logic;
         done : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset_n : std_logic := '0';
   signal parallel_in : std_logic_vector(5 downto 0) := (others => '0');

 	--Outputs
   signal serial_out : std_logic;
   signal done : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: paralelo_serie PORT MAP (
          clk => clk,
          reset_n => reset_n,
          parallel_in => parallel_in,
          serial_out => serial_out,
          done => done
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state
      wait for clk_period;
		parallel_in <= "101101";
      wait for clk_period;		
		reset_n <= '1';
		wait for clk_period;
		parallel_in <= "111001";
      wait until done = '1';
		wait for clk_period;
		wait until done = '1';
		wait until done = '0';
		
		assert false
			report "[OK] Simulacion finalizada"
			severity failure;
   end process;

END;
