--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   17:58:03 01/15/2016
-- Design Name:   
-- Module Name:   E:/Jossy Pastor/Documentos/Universidad/TrabajoSED/trabajo-sed/codigo/Testbench/Multiplexor_tb.vhd
-- Project Name:  ISE_trabajo
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Multiplexor
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Multiplexor_tb IS
END Multiplexor_tb;
 
ARCHITECTURE behavior OF Multiplexor_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Multiplexor
	 
	 GENERIC ( width : positive := 3 );
	 
    PORT(
         a : IN  std_logic_vector(width - 1 downto 0);
         b : IN  std_logic_vector(width - 1 downto 0);
         selec : IN  std_logic;
         salida : OUT  std_logic_vector(width - 1 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(2 downto 0) := (others => '0');
   signal b : std_logic_vector(2 downto 0) := (others => '0');
   signal selec : std_logic := '0';

 	--Outputs
   signal salida : std_logic_vector(2 downto 0);
  
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Multiplexor PORT MAP (
          a => a,
          b => b,
          selec => selec,
          salida => salida
        );



   -- Stimulus process
   stim_proc: process
   begin		
      
		a <= "001";
		b <="010";
		wait for 10 ns;
		
		selec <='1';
		
		wait for 10 ns;
		
		a <="010";
		b <="100";
		
		wait for 10 ns;
		
		selec <='0';
		
		a <="100";
		b <="011";
		
		wait for 10 ns;
		
		selec <='0';
		
		a <= "011";
		b <="101";
		wait for 10 ns;
		
		selec <='1';
		
		wait for 10 ns;
		
		a <="101";
		b <="110";
		
		wait for 10 ns;
		
		selec <='1';
		
		wait for 10 ns;
		
		assert false 
		
		report "Simulacion Finalizada"
		
		severity failure;

   end process;

END;