--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   18:50:34 12/17/2015
-- Design Name:   
-- Module Name:   C:/Users/IAGO/Desktop/ASIGNATURAS/TRABAJO_SED_IAGO/Decodificador_tb.vhd
-- Project Name:  TRABAJO_SED_IAGO
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Decodificador
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Decodificador_tb IS
END Decodificador_tb;
 
ARCHITECTURE behavior OF Decodificador_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Decodificador
    PORT(
         code : IN  std_logic_vector(2 downto 0);
         led : OUT  std_logic_vector(6 downto 0)
        );
    END COMPONENT;
    

   signal code : std_logic_vector(2 downto 0); -- := (others => '0');   signal led : std_logic_vector(6 downto 0);
   SIGNAL led : std_logic_vector(6 DOWNTO 0);
   
		TYPE vtest is record
				code : std_logic_vector(2 DOWNTO 0);
				led : std_logic_vector(6 DOWNTO 0);
		END RECORD;
TYPE vtest_vector IS ARRAY (natural RANGE <>) OF vtest;
	CONSTANT test: vtest_vector := (
		(code => "000", led => "1111110"),
		(code => "001", led => "0110000"),
		(code => "010", led => "1101101"),
		(code => "011", led => "1111001"),
		(code => "100", led => "0110011"),
		(code => "101", led => "1011011"),
		(code => "110", led => "1011111"),
		(code => "111", led => "1110000")
		);
BEGIN
	uut: Decodificador PORT MAP(
		code => code,
		led => led
	);
	tb: PROCESS
	BEGIN
		FOR i IN 0 TO test'HIGH LOOP
			code <= test(i).code;
			WAIT FOR 20 ns;
			ASSERT led = test(i).led
				REPORT "Salida incorrecta."
				SEVERITY FAILURE;
			END LOOP;
		ASSERT false
			REPORT "Simulación finalizada. Test superado."
			SEVERITY FAILURE;

END PROCESS;
END;

-- verificar valores para simulación












