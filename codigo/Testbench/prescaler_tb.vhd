--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:54:14 12/09/2015
-- Design Name:   
-- Module Name:   C:/Users/Tom/Documents/VHDL/TrabajoSED/trabajo-sed/codigo/Testbench/prescaler_tb.vhd
-- Project Name:  ISE_trabajo
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: prescaler
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY prescaler_tb IS
END prescaler_tb;
 
ARCHITECTURE behavioral OF prescaler_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT prescaler
    PORT(
         clk_in : IN  std_logic;
         reset_n : IN  std_logic;
         clk_out : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk_in : std_logic;
   signal reset_n : std_logic; 

 	--Outputs
   signal clk_out : std_logic;

   -- Clock period definitions
   constant clk_in_period : time := 200 ns;
  
BEGIN
	-- Instantiate the Unit Under Test (UUT)
   uut: entity work.prescaler(high_freq)
		PORT MAP (
          clk_in => clk_in,
          reset_n => reset_n,
          clk_out => clk_out
        );

   -- Clock process definitions
   clk_in_process :process
   begin
		clk_in <= '0';
		wait for clk_in_period/2;
		clk_in <= '1';
		wait for clk_in_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      
		wait for clk_in_period*1.2;
		reset_n <= '0';
      wait for clk_in_period*9;	
		reset_n <= '1';
      wait until clk_out = '1';
		wait until clk_out = '0';
		wait until clk_out = '1';
		wait until clk_out = '0';
		wait until clk_out = '1';
		wait until clk_out = '0';
		wait for clk_in_period;
		
		assert false
			report "[OK] Simulacion finalizada"
			severity failure;
   end process;

END;
