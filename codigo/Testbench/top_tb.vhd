--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   19:38:45 01/15/2016
-- Design Name:   
-- Module Name:   C:/Users/IAGO/Desktop/ASIGNATURAS/top/top_tb.vhd
-- Project Name:  top
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: top
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY top_tb IS
END top_tb;
 
ARCHITECTURE behavior OF top_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
    COMPONENT top
    PORT(
		clock 	: in  STD_LOGIC;
		reset_n	: in  STD_LOGIC;
		data		: in  STD_LOGIC;
		pause 	: in  STD_LOGIC;
		direction : in  STD_LOGIC;
		serial_out 		: out STD_LOGIC;
		serial_out_clk : out STD_LOGIC;
		register_clk 	: out STD_LOGIC;
		display 	: out  STD_LOGIC_VECTOR (6 downto 0);
		display_select : out  STD_LOGIC_VECTOR (3 downto 0)
    );
    END COMPONENT;
    

   --Inputs
   signal clock : std_logic := '0';
   signal reset_n : std_logic := '0';
   signal data : std_logic := '0';
   signal pause : std_logic := '0';
   signal direction : std_logic := '0';

 	--Outputs
	signal serial_out 		: STD_LOGIC;
	signal serial_out_clk : STD_LOGIC;
	signal register_clk 	: STD_LOGIC;
   signal display : std_logic_vector(6 downto 0);
   signal display_select : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clock_period : time := 2000 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
  uut: top PORT MAP (
          clock => clock,
          reset_n => reset_n,
          data => data,
          pause => pause,
          direction => direction,
			 serial_out => serial_out,
			 serial_out_clk => serial_out_clk,
			 register_clk => register_clk,
          display => display,
          display_select => display_select
        );

  -- Clock process definitions
  clock_process :process
  begin
  	clock <= '0';
  	wait for clock_period/2;
  	clock <= '1';
  	wait for clock_period/2;
  end process;
 

  -- Stimulus process
  stim_proc: process
  begin		
	reset_n<='0';
	wait for 0.05 sec;
   reset_n <='1';
	pause <='0';
	direction <='1';
	data <='1';
	wait for 1.5 sec;
	pause <='1';
	wait for 0.5 sec;
	pause <='0';
	direction <='0';
	data <='1';
   wait for 1.1 sec;
  ASSERT false
			REPORT "[OK] Simulacion finalizada con exito."
			SEVERITY FAILURE;

END PROCESS;
END;