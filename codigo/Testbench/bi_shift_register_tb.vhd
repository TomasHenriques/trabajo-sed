--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:32:47 01/06/2016
-- Design Name:   
-- Module Name:   C:/Users/Tom/Documents/VHDL/TrabajoSED/trabajo-sed/codigo/Testbench/bi_shift_register_tb.vhd
-- Project Name:  ISE_trabajo
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: bi_shift_register
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY bi_shift_register_tb IS
END bi_shift_register_tb;
 
ARCHITECTURE behavior OF bi_shift_register_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT bi_shift_register
    PORT(
         reset_n : IN  std_logic;
         clk : IN  std_logic;
         mode : IN  std_logic_vector(1 downto 0);
         left_in : IN  std_logic;
         right_in : IN  std_logic;
         left_out : OUT  std_logic;
         right_out : OUT  std_logic;
         q : OUT  std_logic_vector(2 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal reset_n : std_logic := '0';
   signal clk : std_logic := '0';
   signal mode : std_logic_vector(1 downto 0) := (others => '0');
   signal left_in : std_logic := '0';
   signal right_in : std_logic := '0';

 	--Outputs
   signal left_out : std_logic;
   signal right_out : std_logic;
   signal q : std_logic_vector(2 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: bi_shift_register PORT MAP (
          reset_n => reset_n,
          clk => clk,
          mode => mode,
          left_in => left_in,
          right_in => right_in,
          left_out => left_out,
          right_out => right_out,
          q => q
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for clk_period*2.25;	
		reset_n <= '1';
		
		-- desplazamiento a derechas
		mode <= "01";
		left_in <= '1';
		wait for clk_period;
		left_in <= '0';
		wait for clk_period*3;
		mode <= "00";
		wait for clk_period*2;
		mode <= "01";
		wait for clk_period;
		
		-- desplazamiento a izquierdas
		mode <= "10";
		right_in <= '1';
		wait for clk_period;
		right_in <= '0';
		wait for clk_period*3;
		mode <= "00";
		wait for clk_period*2;
		mode <= "10";
		wait for clk_period;
			
		assert false
			report "[OK]: Simulation finished."
			severity failure;
   end process;

END;
