--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:10:59 10/13/2015
-- Design Name:   
-- Module Name:   C:/Users/Tom/Documents/VHDL/tema5_4/counterbin_tb.vhd
-- Project Name:  tema5_4
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: counterbin
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE ieee.numeric_std.ALL;
 
ENTITY ring_counter_tb IS
END ring_counter_tb;
 
ARCHITECTURE behavioral OF ring_counter_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ring_counter
	 GENERIC(
			width : positive :=4
	 );
    PORT(
         clk : IN  std_logic;
         reset_n : IN  std_logic;
         q : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    
	constant counter_width: positive := 4;
	constant counter_states: positive := 4;

   --Inputs
   signal clk : std_logic;
   signal reset_n : std_logic;

 	--Outputs
   signal q : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 100 ns;
	constant delay : time := 50 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ring_counter 
	GENERIC MAP (
			width => counter_width
	)
	PORT MAP (
          clk => clk,
          reset_n => reset_n,
          q => q
    );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
		-- Check reset_n clears output
      wait for 0.25*clk_period;	
		reset_n <= '0';
		wait for delay;
      assert to_integer(unsigned(q)) = 2**(counter_width - 1)
			report "[FAILURE]: Reset not functional."
			severity failure;
		
		-- Check reset_n has priority over clk
		wait until clk = '1';
		wait for delay;
      assert to_integer(unsigned(q)) = 2**(counter_width - 1)
			report "[FAILURE]: Reset not functional."
			severity failure;
		
		-- Check count sequence
		wait until clk = '0';
		reset_n <= '1';
		for i in counter_states downto 1 loop
			assert to_integer(unsigned(q)) = 2**(i - 1)
				report "[FAILURE]: wrong count."
				severity failure;
			wait until clk = '1';
			wait for delay;
		end loop;
		
		-- Terminate testbench
		assert false
			report "[OK]: Simulation finished."
			severity failure;
   end process;

END;
