library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity bi_shift_register is
	generic (
		width : positive := 3
	);
	port ( 
		reset_n  : in  STD_LOGIC;
		clk 	 : in  STD_LOGIC;
		mode 	 : in  STD_LOGIC_VECTOR (1 downto 0);
		left_in  : in  STD_LOGIC;
		right_in : in  STD_LOGIC;
		left_out  : out STD_LOGIC;
		right_out : out  STD_LOGIC;
		q 		 : out  STD_LOGIC_VECTOR (width - 1 downto 0)
	);
end bi_shift_register;

architecture behavioral of bi_shift_register is
	signal q_i : STD_LOGIC_VECTOR (q'range);
begin
	process (reset_n, clk)
	begin
		if reset_n = '0' then
			q_i <= (others => '0');
		elsif clk'event and clk = '1' then 
			if mode = "01" then		-- shift to right			
				q_i <= left_in & q_i(width - 1 downto 1);
			elsif mode = "10" then 		-- shift to left
				q_i <= q_i(width - 2 downto 0) & right_in;
			end if;
		end if;
	end process;
	q <= q_i;
	right_out <= q_i(0);
	left_out <= q_i(width - 1);
end behavioral;